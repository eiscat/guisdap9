function [ SW ] = defaultSwitches( )
%DEFAULTSWITCHES produces a default switch array that turns on all switches

  SW = ones(CIRA.maxSW,1); % all on
  
end

