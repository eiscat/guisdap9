function testTempContinuity(testCase)
  numPoints = 100;
  startAlt = -10;
  endAlt = 500;
  YEAR = 2012;
  xlat = 40;
  xlon = -104;
  hour = 0.0;
  DAYNR = 4.0;
  stl = 0.0;
  sec=hour*3600.;
  f107a = 152.0;
  f107 = 153.0;
  ap = zeros(CIRA.maxAP,1);
  ap(CIRA.DAILY_AP) = 4.1;
  ap(CIRA.CURRENT_AP) = 4.2;
  ap(CIRA.CURRENT_M_3_AP) = 4.3;
  ap(CIRA.CURRENT_M_6_AP) = 4.4;
  ap(CIRA.CURRENT_M_9_AP) = 4.5;
  ap(CIRA.CURRENT_M_12_33_AP) = 4.6;
  ap(CIRA.CURRENT_M_36_57_AP) = 4.7;
  inc = (endAlt-startAlt)/(numPoints-1);
  temps = zeros(numPoints,1);
  dtemps = zeros(numPoints,1);
  d2temps = zeros(numPoints,1);
  d3temps = zeros(numPoints,1);
  d4temps = zeros(numPoints,1);
  press = zeros(numPoints,1);
  dpress = zeros(numPoints,1);
  alts = zeros(numPoints,1);
  ciractx = CIRA();
  ciractx = ciractx.TSELEC(CIRA.allSwitchesOn);
  mass = ciractx.MT(CIRA.ALL_MASS);
  iyd = YEAR *CIRA.YRDSHIFT + DAYNR;
  i = 1;
  for hxx = startAlt:inc:endAlt
    alts(i) = hxx;
    [ D,T,ciractx ] = ciractx.GTD7(iyd,sec,hxx,xlat,xlon,stl,f107a,f107,ap,mass,0.0);
    temps(i) = T(CIRA.TemperatureIndex(CIRA.Der0));
    dtemps(i) = T(CIRA.TemperatureIndex(CIRA.DerAlt));
    d2temps(i) = T(CIRA.TemperatureIndex(CIRA.DerLon));
    d3temps(i) = T(CIRA.TemperatureIndex(CIRA.DerLat));
    d4temps(i) = T(CIRA.TemperatureIndex(CIRA.DerSec));
    press(i) = ciractx.totalPressure(D,T);
    dpress(i) = ciractx.totalPressureGradient(D,T);
    i = i + 1;
  end
  figure;
  semilogy(alts,press);
  hold on;
  title('Pressure and Altitude Derivative of Pressure');
  xlabel('Altitude (km)');
  ylabel('Pressure (mb, mb/km)');
  semilogy(alts,-dpress);
  hold off;
  figure;
  plot(alts,temps);
  hold on;
  title('Altitude Derivative of Temperature');
  xlabel('Altitude (km)');
  ylabel('Temperature (K, K/100km)');
  plot(alts,dtemps*100);
  hold off;
  figure;
  plot(alts,temps);
  hold on;
  title('Longitude Derivative of Temperature');
  xlabel('Altitude (km)');
  ylabel('Temperature (K, K/100deg)');
  plot(alts,d2temps*100);
  hold off;
  figure;
  plot(alts,temps);
  hold on;
  title('Latitude Derivative of Temperature');
  xlabel('Altitude (km)');
  ylabel('Temperature (K, K/100deg)');
  plot(alts,d3temps*100);
  hold off;
  figure;
  plot(alts,temps);
  hold on;
  title('Time Derivative of Temperature');
  xlabel('Altitude (km)');
  ylabel('Temperature (K, K/day)');
  plot(alts,d4temps*86400);
  hold off;
  testCase.verifyEqual(hxx,endAlt);
end
