function testAltDerivative(testCase)
  numPoints = 100;
  hxx = 10;
  YEAR = 2012;
  xlat = 40;
  xlon = -104;
  hour = 0.0;
  DAYNR = 4.0;
  stl = 0.0;
  sec=hour*3600.;
  f107a = 152.0;
  f107 = 153.0;
  ap = zeros(CIRA.maxAP,1);
  ap(CIRA.DAILY_AP) = 4.1;
  ap(CIRA.CURRENT_AP) = 4.2;
  ap(CIRA.CURRENT_M_3_AP) = 4.3;
  ap(CIRA.CURRENT_M_6_AP) = 4.4;
  ap(CIRA.CURRENT_M_9_AP) = 4.5;
  ap(CIRA.CURRENT_M_12_33_AP) = 4.6;
  ap(CIRA.CURRENT_M_36_57_AP) = 4.7;
  ddenss = zeros(numPoints,1);
  dtemps = zeros(numPoints,1);
  d2temps = zeros(numPoints,1);
  d2denss = zeros(numPoints,1);
  alts = zeros(numPoints,1);
  maxEx = 4;
  minEx = 1;
  incEx = (maxEx-minEx)/(numPoints+1);
  ciractx = CIRA();
  ciractx = ciractx.TSELEC(CIRA.allSwitchesOn);
  mass = ciractx.MT(CIRA.ALL_MASS);
  iyd = YEAR *CIRA.YRDSHIFT + DAYNR;
  [ D0,T0,ciractx ] = ciractx.GTD7(iyd,sec,hxx,xlat,xlon,stl,f107a,f107,ap,mass,0.0);
  i = 1;
  for ex = minEx:incEx:maxEx
    del = abs(hxx)*10.0^(-ex);
    alts(i) = del;
    [ D,T,ciractx ] = ciractx.GTD7(iyd,sec,hxx+del,xlat,xlon,stl,f107a,f107,ap,mass,0.0);
    dtemps(i) = T(CIRA.TemperatureIndex(CIRA.DerAlt));
    d2temps(i) = (T(CIRA.TemperatureIndex(CIRA.Der0))-T0(CIRA.TemperatureIndex(CIRA.Der0)))/del;
    ddenss(i) = D(CIRA.N2_GDENS);
    d2denss(i) = (D(CIRA.N2_DENS)-D0(CIRA.N2_DENS))/del;
    i = i + 1;
  end
  figure;
  semilogx(alts,dtemps);
  hold on;
  title('Altitude Derivative of Temperature');
  xlabel('fraction from altitude');
  ylabel('Temperature Gradient (K/km)');
  semilogx(alts,d2temps);
  hold off;
  figure;
  semilogx(alts,ddenss);
  hold on;
  title('Altitude Derivative of Density');
  xlabel('fraction from altitude');
  ylabel('N2 Number Density Gradient (1/cm^3/km)');
  semilogx(alts,d2denss);
  hold off;
  testCase.verifyEqual(hxx,hxx);
end
