% glob_design: Global definitions for the design routines
% GUISDAP v.1.60 96-05-27 Copyright Asko Huuskonen and Markku Lehtinen
%
global ch_p ch_adcint ch_filter ch_fradar ch_gain

global lp_t1 lp_t2 lp_dt lp_nt lp_vc lp_ra lp_ri lp_T lp_code lp_bcs lp_h 
global lp_nfir lp_fir lp_dec  lp_ind 
 
global p_dtau  p_rep p_ND p_XMITloc p_RECloc p_calTemp ra_prev ra_next

global vc_ch vc_t1 vc_t2 vc_adcint vc_p vc_env vc_envo vc_group 
global vc_sampling vc_mf vc_number vc_next

global bm_next code_next type_next
