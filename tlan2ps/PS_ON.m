% PS_ON.m: ps file interpreter routine
% GUISDAP v1.60   96-05-27 Copyright Asko Huuskonen, Markku Lehtinen
%
% [ch t1 t2] ;PS_ON
r=ans;
td_ind=td_ind+1;
td_ch(td_ind)=r(1);
td_t1(td_ind)=r(2);
td_t2(td_ind)=r(3);
td_am(td_ind)=2;
