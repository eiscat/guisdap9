% PS_SETADCINT.m: ps file interpreter routine
% GUISDAP v1.60   96-05-27 Copyright Asko Huuskonen, Markku Lehtinen
%
% [ch dt] ;PS_SETADCINT
r=ans;
ch_adcint(r(1))=r(2);
