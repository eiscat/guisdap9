% res=col(A): Makes a vector or a matrix into a column vector.
% GUISDAP v.1.60 96-05-27 Copyright Asko Huuskonen and Markku Lehtinen
%
  function res=col(A)
%
  res=A(:);
