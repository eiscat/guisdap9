% glob_GUP.m: declares all the global variables needed to run init_GUP
% GUISDAP v.1.60 96-05-27 Copyright Asko Huuskonen and Markku Lehtinen
%
% See also: globals, start_GUP, init_GUP
%
global ch_adcint ch_filter ch_fradar ch_gain p_XMITloc p_RECloc

global lp_t1 lp_t2 lp_dt lp_nt lp_vc lp_ra lp_ri 
global lp_T lp_code lp_bcs lp_h lp_nfir lp_fir lp_dec 
global lpg_lag lpg_dt lpg_nt lpg_ra lpg_ri lpg_T lpg_code lpg_bac lpg_cal
global lpg_bcs lpg_h lpg_w lpg_ND lpg_wom lpg_wr lpg_lpdata lpg_lpstart lpg_lpend
 
global k_radar0 p_om p_dtau p_T0 p_N0 p_D0 p_m0 p_om0 p_R0 p_rep

global vc_ch vc_p vc_env vc_envo vc_Aenv vc_Ap vc_penv vc_Apenv vc_penvabs vc_penvo vc_group
global vc_sampling
