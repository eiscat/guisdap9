% nat_const.m: script defining useful natural constants:
% GUISDAP v.1.60 96-05-27 Copyright Asko Huuskonen and Markku Lehtinen
%
% Values updated on 2020-04-28 (AT)
%
% v_lightspeed=299792458;
% v_Boltzmann=1.38064852e-23;
% v_electronmass=9.10938356e-31;
% v_amu=1.66053904e-27;
% v_electronradius=2.81794032e-15;
% v_epsilon0=8.85418782e-12;
% v_elemcharge=1.60217662e-19;

global v_lightspeed v_Boltzmann v_electronmass v_amu v_electronradius v_epsilon0 v_elemcharge

v_lightspeed=299792458;
v_Boltzmann=1.38064852e-23;
v_electronmass=9.10938356e-31;
v_amu=1.66053904e-27;
v_electronradius=2.81794032e-15;
v_epsilon0=8.85418782e-12;
v_elemcharge=1.60217662e-19;
