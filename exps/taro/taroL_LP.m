N_SCAN=32*6;

COR_init((360+221)*N_SCAN,31)
for vc=1:6:N_SCAN
  COR_pp(0,1,vc,'s',1,178,0,1)
  COR_fraclp(178,vc,'s',178-2*15,16,50,(1:31)*25,1)
  COR_pp(7468,1,vc+1,'s',1,178,0,2)
  COR_fraclp(7468+178,vc+1,'s',178-2*15,16,50,(1:31)*25,2)
  COR_fdalt(14936,vc+1,'s',0,0,14,16,50,148,(0:16)*25,2)
end
for vc=3:6:N_SCAN
  COR_pp(0,1,vc,'s',1,178,0,1)
  COR_fraclp(178,vc,'s',178-2*15,16,50,(1:31)*25,1)
  COR_pp(7468,1,vc+1,'s',1,178,0,2)
  COR_fraclp(7468+178,vc+1,'s',178-2*15,16,50,(1:31)*25,2)
  COR_fdalt(14936,vc+1,'s',0,0,14,16,50,148,(0:16)*25,2)
  if rem(vc,12)<6
    COR_pp(15190,1,vc,'b',1,178,0,1)
    COR_pp(15190+178,1,vc,'c',1,12,0,1)
  else
    COR_pp(15190+190,1,vc+1,'b',1,178,0,2)
    COR_pp(15190+190+178,1,vc+1,'c',1,12,0,2)
  end
end
for vc=5:6:N_SCAN
  COR_pp(15826,1,vc,'b',1,178,0,3)
  COR_pp(15826+178,1,vc,'c',1,12,0,3)
  COR_pp(16144,1,vc,'s',1,178,0,3)
  COR_fraclp(16322,vc,'s',178-2*15,16,50,(1:31)*25,3)
  COR_pp(23612,1,vc+1,'b',1,178,0,4)
  COR_pp(23612+178,1,vc+1,'c',1,12,0,4)
  COR_pp(23930,1,vc+1,'s',1,178,0,4)
  COR_fraclp(24108,vc+1,'s',178-2*15,16,50,(1:31)*25,4)
  COR_fdalt(31398,vc+1,'s',0,0,14,16,50,148,(0:16)*25,4)
end
COR_end
