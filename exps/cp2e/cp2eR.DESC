%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                            %
%        C P - 1 - K - T  : D E S C                                          %
%                                                                            %
%        Gudmund Wannberg, EISCAT HQ,                                        %
%                           Updated June 19, 1992 (MTR)                      %
%                           Updated July 30, 1992 (UGW): PP ranges corrected %
%                      Last updated 930215, adapted from CP-1-J:DESC (PNC)   %
%                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IN THE EXPERIMENT THE ELAN,TLAN AND CLAN FILES ARE "MATCHED" TOGETHER.
CARE SHOULD THEREFORE BE EXERCISED IF THE EXPERIMENT WERE GOING TO BE
MODIFIED EVEN SLIGHTLY.

1. General information

CP-1-K is identical with CP-1-J except that the remote antennas point only to
the F-region. The CP-1-J experiment is the first of a next generation of codes.
It makes extensive use of the new signal processing hardware which has
been installed in the EISCAT system over the past two years. The E region
data rate is increased by about 20 % in the power profile part and by as
much as 100 - 200 % in the spectral data. This is accomplished at the cost
of a minor reduction in Troms| F region statistics (about -15 %), which is
caused mainly by the transmission of only one long pulse/IPP rather than
two, as in the previous CP-1 versions. So as not to compromise the tristatic
velocity statistics, the remote sites are programmed to receive both the
long pulse and also the alternating code transmission which produces the
E region spectral data in Troms|. This causes a net increase of the number
of spectral estimates per unit time at the remote sites of about 70 % relative
to the CP-1-I.

  The integration time is set to 5 seconds, which may be useful in highly
dynamic situations (auroral substorms etc.). IT MUST NOT BE CHANGED NEITHER
FROM THE EROS CONSOLE NOR THROUGH A CHANGE TO THE 'START-RADAR' COMMAND IN THE
:ELAN FILE; ANY CHANGES WILL CAUSE THE ALTERNATING CODE PART OF THE EXPERIMENT
TO FAIL. IF A DIFFERENT INTEGRATION TIME IS DESIRED THE TARLAN CODE GENERATOR
PROGRAM "ATC-1" MUST BE USED TO CREATE A NEW :TLAN FILE ADAPTED TO THE NEW
INTEGRATION TIME !!


2. System configuration

  This experiment uses all eight channels of the receiver. The PDF filters
should be installed so:
                                                              Used for

CH1 and CH2       Linear phase, 25 kHz BW, lower crate       F region PP

CH3 and CH4       Butterworth,  25 kHz BW, upper crate       F region LPACF

CH5 to  CH8       Linear phase, 25 kHz BW, lower crate       D and E reg PP
                                                             + alt. codes

  The three channel groups should be internally balanced for gain to within
one decibel. It is permissible to completely attenuate out faulty channels
within a group in an emergency. This causes a data loss of 50 % per channel
in the F region groups but only a 25 % per channel loss in the D,E region
group.

  In every IPP pulses are transmitted on four different frequencies, i.e.
one frequency each for the FPP and FACF groups and two frequencies for
the D,E region group (one for the PP and the other for the alternating code).
In the next cycle, all frequencies are changed and the pattern is repeated,
now of course using different receiver channels. Towards the end of each
cycle, all channels which are due for use in the next cycle are calibrated.

  A new member of the 16 baud strong condition alternating code set is
transmitted every two IPPs, so it takes 64 IPPs to complete the set. At the
end of such a set, the frequency/channel pair used for the D,E region PP is
interchanged with the pair used for the alternating code (this happens about
twice a second). The result of this operation is that the D,E PP and the
ACFs measured with the alternating code are automatically intercalibrated
in an average sense. Decoding of the alternating code data is fully automatic
and performed by special purpose hardware in the correlator, which is
partly controlled from the correlator microprogram and partly from the
radar controller (the :TLAN and :TCOD files contain special 'bit' commands
to this effect).

CAUTION: Because of the intricate commutations of channels and frequencies
used in the experiment, it is inadvisable to attempt to change a frequency
or channel by editing the :TLAN file. A special "TARLAN generator" program
has been written for this purpose. It is still not quite debugged and should
be used only in cooperation with the author, UGW at EISCAT HQ, who will be
pleased to offer help and advice.


3. Data dump structure, CP-1-J-T

  The data dump contains five different blocks of data, namely

                   i) An ungated power profile for the D and E regions,
                      also serving as zero lags for the ACFs in block (iii),
                  ii) A "variance profile" computed from the undecoded
                      samples taken from the alternating code modulation,
                 iii) Incomplete ACFs derived from the alternating code,
                  iv) A gated, coarse resolution power profile for the
                      F region,
                   v) Conventional long pulse ACF data.

   The dump is 2036 complex double integers long ; the SCANCOUNT value is
stored in dump address 2035.



3.1  D and E region power profile block

  This block contains ungated power profile data which also doubles as
the zero lags for the ACFs stored in block (iii). Specifically, there are

     90  (S+B) gates    at addresses 0000-0089
     60    B   gates    at addresses 0090-0149
     10  (B+C) gates    at addresses 0150-0159

  Gate no. 9 at address 0008 coincides in range with A/C ACF gate no. 1
in block (iii). Data from two frequencies/channels are added; this is the
only block in the dump that employs channel adding!


3.2  Variance profile data block

  This block contains 76 power estimates at addresses 0160-0235. Its use
is further described below.


3.3. Alternating code ACF block

  This block is very straightforward. It contains

     61  S gates (lags 1-15) at addresses 0236-1150, IT weighting, Ns = 16
      2  B gates (lags 1-15) at addresses 1151-1180, IT weighting, Ns = 16

      IT = inverse triangular, w(l) = Ns - l  where l is the lag index

  No channel adding is applied (one channel only per IPP).

NOTE: these gates contain NO zero lag - it cannot be unambiguously
estimated from the alternating code. Note also that the mean of the back-
ground is zero if the algorithm works properly, so background subtraction
is neither necessary nor recommended in this block (it would just increase
the variance !).

  The RTGRAPH has been upgraded so that if <NO. OF SKIP WORDS>:=-1
a zero lag is extrapolated from the data and the ACF displayed with this
included. This allows you to display also power spectra with a proper
baseline level. The present :GDEF file uses this feature.


3.4. F region power profile block

  This block contains (S+B), B and (B+C) gates that exactly match those
of the CP-1-I-T low resolution power profile in gate separation and range
extent. The start range, the P-P resolution and the order of the subblocks
are different:

      80 (S+B) gates  at addresses 1181-1260
      40   B   gates  at addresses 1261-1300
       6 (B+C) gates  at addresses 1301-1306

  Gate adding is used; three neighbouring estimates sampled 10 us apart
are added to form each gate in the output. No channel adding is applied.


3.5. Long pulse ACF block

   The gating parameters used in this block are identical to those of the
CP-1-I-T LPACF, which should facilitate the adaption of existing
analysis software to the new program. Specifically, the block contains

   21 S+B gates (lags 0-25) at addresses 1307 - 1852, GS weighting, Ns = 15,
    6  B  gates (lags 0-25) at addresses 1853 - 2008, GS weighting, Ns = 15,
    1 B+C gate  (lag 0    ) at addresses 2009 - 2034, GS weighting, Ns = 15.

      GS = Gen-System ,        w(l) = Ns + l  where l is the lag index

    NO adding of channels is used in this block.

NOTE: the only real difference between this data block and the output of
a CP-1-I-T experiment is the ORDER of the sub-blocks, plus the fact that
only one channel is received per IPP.


4. Data properties

4.1. D/E region power profile

 This is a very simple power profile data vector. It is derived from two
21 us pulses transmitted immediately after each other on two different
frequencies in the first half of each IPP. The frequency used for the first
pulse is later used for the alternating code which is transmitted in the
second half of each IPP. The frequencies are commutated according to a
relatively complicated scheme from one IPP to the next, but this ensures
that all frequencies used for the PP pulses are eventually also used for
the alternating code; hence the power profiles and the ACFs computed from
the code are automatically gain equalised and the PP can be used as a zero
lag profile for the ACF series (which does not contain lag 0).

  The PP channels are sampled at a 1/21 us rate and, as mentioned above,
two channels are added in the dump. No gate adding is performed. The
filters are 25 kHz linear phase ones. The range coverage is 63.70 - 344.05
km and the range increment/gate is 3.15 km. The start range has been computed
using the actual measured impulse response of the 25 kHz LP filters used and
it is thus a few hundred meters less than what one would have deduced by using
a simple model where the impulse response of the filter maximises at t=(1/BW)
(the start range computed from the simple model is 64.65 km, which has been used
in the :GDEF and :DESC files prior to 1992-07-30 - please note the change).

  There are two things worth mentioning in connection with this power profile.
Firstly, the background is derived only from samples taken on one channel,
so the weight of the background estimate is nominally equal to unity - this
arrangement was chosen so as to avoid getting any signal from far out ranges
into the background gates. However, the actual weight of the signal is slightly
less than two, because in the very first cycle after a data dump, the estimates
from the second PP channel are overwritten by those from the first channel.
If the scancount is N, there are thus (2*N-1) (S+B) estimates and N background
estimates per gate per dump. For the case of a 5 seconds integration time,
N is equal to 640 and there are 1279 (S+B) estimates and 640 (B) estimates per
gate. If the nominal weights of 2 and 1 are used (as e.g. in the RTGRAPH),
the background is overestimated by some 0.75 10**-3. In a careful analysis,
this must be taken into account !

  Secondly, the background estimate is stored as a time series of 60 points,
rather than as a compacted estimate. The background sampling starts at a
point in time which is immediately before the point at which the first D-
region (S+B) point is sampled, and it then continues for 1260 us. During this
time, the receiver gain drops by several percent, although most of the gain
drop occurs within a very short time immediately following the RXPOFF
command. The gain variation is a consequence of the receiver switching method
used in Troms| at present and cannot be suppressed much more than to its
present magnitude by any hardware changes. Incidentally, there has probably
always been a cyclic gain variation of this kind in the Tromso receiver, but
prior to the front end change in 1988 it used to go in the opposite direction,
i.e. the gain increased with time into a receive cycle. This is not as notice-
able on a raw data display as the present effect, because it tends to bias
the data into a physically "expected" direction, i.e. rapidly reducing density
when going downwards below the E region. The present effect can result in
something which appears as a residual density below 80 km, and negative
densities at far ranges, if the background which is subtracted from the (S+B)
gates is a plain average over the entire IPP. Now, since the background is
available as a time series, one can use different strategies for its removal.
One possibility is to use local averages, centered in time on each (S+B) gate,
another possibility is to use a fitted time-varying background estimate.
If the first ten background points are excluded, the rest can be fairly
accurately represented by a straight line fit.

 Since the gain variation is a function of elapsed time after the receiver
has been switched on by the RXPOFF command, everything must be related to
this point when the background estimate is calculated. If the RXPOFF command
is executed at some time  t, the background sampling starts at (t+260) us
and the first signal sample is taken at (t+370) us, i.e. about five samples
later in relative time.


4.2. Variance profile

  This data block is included in the hope that it may become useful in the
analysis of the alternating code ACF data. It contains a series of raw
power estimates derived from all the samples used by the decoding process
(there are 76 of them in this case of a 16 bit code and 61 gates) and so
it can be understood as an "ambiguous lag-0 profile". An advanced analysis
routine might attempt to estimate the true variance of each lagged product
in the decoded ACFs by starting from this ambiguous power profile.

  The first point in the variance profile corresponds to a sample taken
as the leading edge of the coded pulse passes 90 km range. As the modulation
is 336 us long, the power in this point is an average over 50.4 km, and
the lower boundary of this first point is just below 40 km range. Some
ground clutter may be expected to occur in the first few points. This,
if present, will also affect the first few ACF gates. It remains to be
seen to what degree this corrupts the data.


4.3. Alternating code ACF

 The alternating-code ACF data is derived from a 16 baud, strong condition
alternating code set using a baud length of 21 us. A total of four different
frequencies and four receiver channels are used for this modulation, and
each of the 32 sequences that make up the code set is repeated  on
two different frequencies. It takes slightly less than one-half second
to complete the full set; this is consequently also the time during which
the target (ie the ionosphere) must be assumed to be stationary in an
average sense, for the technique to produce dependable results.

  Sampling is at 21 us/sample, filters used are 25 kHz linear phase.
The lag extent is from 21 - 315 us and the lag increment 21 us.

  Fitting for velocity should be straightforward - the zero lag is not
used anyway in the velocity estimator. Fitting for density requires an
estimate of the zero lag. This can be taken from the power profile in
block (i), which is measured through the same receiver channels and on the
same frequencies as the alternating code - it is consequently automatically
intercalibrated with the nonzero lags and can be patched into the zero lag
locations of the ACFs after proper scaling. The relative weight of the PP
is 2, but remember that it must be background subtracted also !

  PP gate no. 9 corresponds to ACF gate no. 1, which is at 89.00 km range.
The range increment/gate is 3.15 km.


4.4. F region power profile

  This power profile is mainly intended as a replacement for the 2*29 us
power profile in the previous CP-1-I-T. The number of gates, range coverage
and gating all are identical to the CP-1-I version. The modulation is
slightly different - as there is more freedom with respect to how long
the transmit pulses can be made in CP-1-J, we have decided to use just
one, 40 us pulse per IPP rather than the two, 29 us pulses of CP-1-I. The
p-p range resolution of this modulation is thus slightly coarser than
in the previous version, about 9 km, but the FWHM is only marginally in-
creased. Only one channel per IPP is used. NOTE: The starting range of
this PP is 70.35 km. This is also computed using the actual impulse response
of the 25 kHz LP filters used.


4.5. Long pulse ACF

  The signal carrying part of this block is arranged to behave exactly
the same as the CP-1-I-T, i.e. the series of gates is computed with a
volume index of 15 and a max. lag of 25. The first gate is centered on
150 km range. Six background gates are computed instead of five; they
are all fully independent.

 The noise calibration part of this data is quite different from that of
CP-1-I - it contains a computed zero lag with the proper weight relative
to the other gates, but all higher lags are forced to zero in the output.
There is a reason for this: If one computes only the zero lag, only 15
noise samples are required, but if one were to compute the full ACF from
samples taken during the noise injection period, 73 samples would be
required! Omitting everything but the zero lag computation thus enables
us to save a considerable amount of time, namely (73-15)*10 us = 580 us,
in the calibration phase of the experiment. This represents a significant
increase in the effective duty cycle of the experiment. At the same time,
the output format is consistent with that of the other ACFs in the block.
Note, however, that in RTGRAPH it is impossible to display the calibration
gate using background subtraction - all lags except lag 0 will come out
negative in this case......

 The sampling increment is 10 us, hence the lag extent is from 0 - 250 us.
25 kHz Butterworth filters with a nominal risetime of 24 us are used, which
leads to oversampling and a substantial correlation between neighbouring lags.

                            REMOTE SITE
                            ************
5. Data dump structure, CP-1-J-R

  The data dump contains three blocks of data with somewhat different
structures, namely

                   i) Conventional long pulse ACF data,
                  ii) Incomplete ACFs derived from the alternating code,
                 iii) Boxcar weighted full ACFs for calibration of the
                      channels used for the alternating code.

   The dump is 394 complex double integers long ; the SCANCOUNT value is
stored in dump address 393.


5.1. Long pulse ACF block

   The basic structure of this block is identical to that of the previous
CP-1-I-R data dump, which should facilitate the adaption of existing analysis
software to the new program. Specifically, the block contains

    5 S+B gates (lags 0-29) at addresses 0000 - 0149, IT weighting, Ns = 32,
    2  B  gates (lags 0-29) at addresses 0150 - 0209, BC weighting, Ns = 320,
    1 B+C gate  (lags 0-29) at addresses 0210 - 0239, BC weighting, Ns = 320.

      IT = inverse triangular, w(l) = Ns - l  where l is the lag index
      BC = boxcar,             w(l) = Ns for all l.

    NO adding of channels occurs in the (S+B) part of the data.
    Two channels are added in the calibration and background, but this
    adding is implied in the assigned weight Ns = 320, so no action is
    necessary in the analysis routines.

NOTE: the only real difference between this data block and the output of
a CP-1-I-R experiment is the WEIGHTING of the background and calibration
gates, plus the fact that only one channel is received per IPP.


5.2. Alternating code ACF block

  This block is very straightforward. It contains

     7  S gates (lags 1-15) at addresses 0240-0344, IT weighting, Ns = 16

  No channel adding is applied (one channel only per IPP).

NOTE: these gates contain NO zero lag - this cannot be unambiguously
estimated from the alternating code. Note also that the mean of the back-
ground is zero if the algorithm works properly, so background subtraction
is neither necessary nor recommended in this block (it would just increase
the variance !).


5.3. Boxcar ACF block

  This block contains background and noise injection gates necessary for
a proper analysis of the data in the alternating code ACF block:

    2   B  gates (lags 0-15) at addresses 0345-0376, BC weighting, Ns = 80,
    1  B+C gate  (lags 0-15) at addresses 0377-0392, BC weighting, Ns = 80.



6. Data properties

6.1. Long pulse ACF

  The signal carrying part of this block is arranged to behave exactly
the same as the CP-1-I-R, i.e. the series of gates is computed with a
sample space overlap of 10 samples, and the third gate should be properly
illuminated if the second and fourth gates are of about equal magnitude.
All analysis should be performed on the third gate only. This is also
the one to use for SNR and real time velocity estimates in RTGRAPH.

 The sampling increment is 10 us, hence the lag extent is from 0 - 290 us.
25 kHz Butterworth filters with a nominal risetime of 21 us are used, which
leads to oversampling and a substantial correlation between neighbouring lags.


6.2. Alternating code ACF

  The seven ACFs in this block are arranged to fall symmetrically around
the nominal intersection of the Troms| beam with the remote site beams.
As the sampling rate is 1/(21 us) and the filter impulse response is matched
to this, the spatial response is such that the gates are separated along
the scattering bisector by (3.15 cos @/2), where @ is the full scattering
angle. This distance is typically of the order of 3 km in the F region,
which is only about one half of the FWHM of the bistatic illumination
distribution, defined by the spatial overlap of the two beams, so there
will be useable signal power in at least the centermost three gates under
most conditions. Furthermore, the properties of the alternating code
algorithm ensure that the illumination is the same at all lags for all
gates - there is no skewness as is the case in the long pulse ACF. There
may therefore be some advantage in simply adding a few gates together be-
fore fitting the data.

  Fitting for velocity should be straightforward - the zero lag is not
used anyway in the velocity estimator. Fitting for density requires an
estimate of the zero lag. This may be extrapolated from the measured ACF
and intercalibrated with the long pulse ACF data using the calibration
values in data block 3 (see below).

  Sampling is at 21 us/sample, filters used are 25 kHz linear phase.
The lag extent is from 21 - 315 us and the lag increment 21 us.


6.3. Boxcar background/calibration ACF block

   An estimate of the channel gain (ie its numerical output per noise
injection sample) of the channels used for the alternating code modulation
is necessary for a proper density and temperature analysis. Since the
decoding algorithm suppresses the mean of any stationary, uncoded signal
component in the data, it also suppresses the background noise and the
calibrated noise injection, so no gain estimates can be derived from
block 2. This is the reason for including block 3 - it is computed using
conventional boxcar-weighted ACFs with no decoding applied and so it
provides zero lags both for the background noise as well as for the
(background + calibration) noise. Sampling rate and filters are identical
with those used in block 2.

   Since the alternating code channels are fitted with linear phase filters
and sampled at exactly the Shannon rate, the passband shape as derived
from an FFT applied to the ACF data should appear totally flat. Any
deviation from a flat spectrum indicates trouble and must be investigated.


7. Additional caveats

  As indicated above, the data dump contains two types of data representing
the state of the same target volume. They cannot be added before analysis,
but it should be possible to add the fitted parameters in a weighted manner
to reduce their variances substantially - this is especially true of the
velocity, whose mean does not depend on the absolute power received.
The gain in statistics depends directly on the assumptions made in the
adding algorithm and cannot be specified here, but for velocity it may
approach 50 % above the previous CP-1-I-R.

  IT IS ULTIMATELY THE END USER WHO MUST DECIDE HOW TO ADD AND WEIGHT
THE TWO DATA SETS AND ESTIMATE THE GAIN IN STATISTICS IN A GIVEN SET OF
CIRCUMSTANCES !

8.    GEOMETRICAL PROPERTIES
      **********************

The Tromso antenna is kept fixed parallel to the magnetic field at the
centre of range gate 7 (285.0 km range; 278.6 km altitude). The remote
sites make continuous measurements at this height.
