%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   ***  E R O S   P R O G R A M M E   F O R   T R O M S O  ***
?
?    =======================
?    C P - 1 - K - T
?    =======================
?
%     FILE: (CP)CP-1-K-T:ELAN (VERSION 930215)
%
%      WRITTEN BY GUDMUND WANNBERG
%      EISCAT HQ , FEBRUARY 1991
%
%      A GENERAL PURPOSE PROGRAM FOR AURORAL RESEARCH
%      BASED ON SP-EI-ALTCP1-T BUT USING ALTERNATING CODES FOR THE E REGION DATA.
%      DECODING OF THE ALTERNATING CODES PERFORMED IN THE CORRELATOR.
%
%      MODULATIONS: 1 LONG PULSE OF 350 US DURATION
%                   1 LOW RESOLUTION POWER PROFILE OF 40 US
%                   2 HIGH RESOLUTION POWER PROFILE PULSES USED AS
%                     D- AND E- LAYER POWER PROFILES  AND AS ZEROLAGS
%                     IN LOW ALTITUDE ACF DATA, PULSE LENGTH 21 US.
%                   1 16x21 US MEMBER OF A STRONG CONDITION ALTERNATING CODE
%                     SET (CONTAINING 32 MEMBERS)
%                     USED FOR E AND F1 REGION SPECTRAL MEASUREMENTS.
%                     PULSE LENGTH 21 US. LAG INCREMENT 21 US, LAGS 1-15
%                     DECODED BY HARDWARE DECODER IN THE CORRELATOR.
%      BEAM IS FIELD ALIGNED AT 278.6 KM (IGRF MODEL 1985-1990, NOV 1986)
%
%      DOPPLER CALIBRATION IS INCLUDED AS BLOCK 2
%      NOTE THAT DOPPLER CAL. IS INVISIBLE IN ALTCODE DATA VECTOR!
%      BLOCK 3 MAY BE USED TO SWITCH BACK FROM CALIBRATION TO EXPERIMENT
%
%      REVISION HISTORY:
%
%      1992-05-22 All frequencies changed to avoid plasma line contamination
%                 of D/E PP by long pulse ACF transmission. Old frequency list
%                 retained in program as comments (for completeness)./UGW
%
%
%      1992-12-01 A fault in the alternating code sequence, affecting bit 7
%                 in scans 9 - 24 inclusive has been corrected in the :TLAN
%                 file. Previous versions of the :TLAN produce data with slight
%                 weighting skewness, which gives about 15 % too low density
%                 when analysed assuming correct weighting - but no range
%                 ambiguities and only insignificant effects on the Te and Ti.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SPARC-TRANSFER Y
WRITE-FILE (CP)CP-1-K-T:ELAN
WRITE-FILE (CP)CP-1-K-T:GDEF
WRITE-FILE (CP)CP-1-K:DESC
WRITE-FILE (CP)CP-1-J-T:TLAN
WRITE-FILE (CP)CP-1-J-T:CLAN
%   HOLD 10
SET-PARAMETER R1,278.6        % DEFINE INTERSECTION ALTITUDE IN F-REGION

   CHANGE-REF-SITE TROMSO,,,,,
   POINT-REF-H 182.57,77.54,R1   % FIELD ALIGNED FOR RANGE 285KM (GATE 7)

SET-SIG-PATH ALLX

SYNC -360
LOAD-CORR CP-1-J-T:CCOD             %A SPECIAL CORRELATOR PROGRAM
                                    %MATCHED TO USED ELAN AND TLAN FILES
HOLD 10

LOAD-RA-CON CP-1-J-T:TCOD
HOLD 10

TRANSFER-NOISE-CONT RADAR

SET-LO1 812.0        % as a reminder only, value fixed in HW / 6.9.90 jm

%                          !Here follows a list of old frequencies (pre-920522):
%                          !
SET-FREQ 1, F4  %LO2=147.5 ! SET-FREQ 1, F4  %LO2=147.5
SET-FREQ 2, F11 %LO2=151.0 ! SET-FREQ 2, F5  %LO2=148.0
SET-FREQ 3, F7  %LO2=149.0 ! SET-FREQ 3, F0  %LO2=145.5
SET-FREQ 4, F8  %LO2=149.5 ! SET-FREQ 4, F1  %LO2=146.0
SET-FREQ 5, F5  %LO2=148.0 ! SET-FREQ 5, F6  %LO2=148.5
SET-FREQ 6, F9  %LO2=150.0 ! SET-FREQ 6, F7  %LO2=149.0
SET-FREQ 7, F6  %LO2=148.5 ! SET-FREQ 7, F8  %LO2=149.5
SET-FREQ 8, F10 %LO2=150.5 ! SET-FREQ 8, F9  %LO2=150.0

SET-SIG-ATT X,0           % CHANGED FROM 3 910115 MTR

SET-CH-ATT 1,2             %
SET-CH-ATT 2,3             %
SET-CH-ATT 3,5
SET-CH-ATT 4,4
SET-CH-ATT 5,3             %
SET-CH-ATT 6,0
SET-CH-ATT 7,1             %
SET-CH-ATT 8,6             % CHANNELS BALANCED 910221 - UGW
%                          % - MUST BE RE-CHECKED NOW AND THEN
SYNC 10

SET-FILTER 1,LI,25.0,L
SET-FILTER 2,LI,25.0,L
SET-FILTER 3,BU,25.0,U
SET-FILTER 4,BU,25.0,U
SET-FILTER 5,LI,25.0,L
SET-FILTER 6,LI,25.0,L
SET-FILTER 7,LI,25.0,L
SET-FILTER 8,LI,25.0,L

% ------------------

SELECT-ALTERNATING-CODE   16      % 16 BAUD STRONG CONDITION CODE USED

SET-COR-APB 15,15    % MAX. LAG IN THE ALTERNATING CODE GATES
SET-COR-APB 14,61    % NO. OF (S+B) GATES IN THE ALTERNATING CODE DATA VECTOR
SET-COR-APB 13, 2    % NO. OF (B)   GATES IN THE ALTERNATING CODE DATA VECTOR
SET-COR-APB 12,90    % NO. OF (S+B) DEPP GATES
SET-COR-APB 11,60    % NO. OF (B)   DEPP GATES
SET-COR-APB 10,10    % NO. OF (B+C) DEPP GATES
SET-COR-APB  9,21    % NO. OF (S+B) LPACF GATES
SET-COR-APB  8, 6    % NO. OF (B)   LPACF GATES
SET-COR-APB  7, 1    % NO. OF (B+C) LPACF GATES (ONLY THE ZERO LAG COMPUTED!)
SET-COR-APB  6,25    % LPACF MAX. LAG
SET-COR-APB  5,15    % LPACF VOLUME INDEX
SET-COR-APB  4,378   % NO. OF SAMPLES IN FPP VECTOR

% APB 1 - APB 3 ARE WORKING REGISTERS

SET-COR-APB  0,1     %APBINCR

HOLD 10

SET-COR-APM 15,15    % NO. OF LAGS/GATE IN THE ALTERNATING CODE DATA VECTOR

SET-COR-APM 14, 236  % A/C (S+B) DATA, 61*15 POINTS
SET-COR-APM 13,1151  % A/C (B)   DATA,  2*15 POINTS

SET-COR-APM 12,   0  % DEPP (S+B) DATA, 90 POINTS
SET-COR-APM 11,  90  % DEPP (B)   DATA, 60 POINTS
SET-COR-APM 10, 150  % DEPP (B+C) DATA, 10 POINTS

SET-COR-APM  9, 1307 % LPACF (S+B) DATA, 21*26 POINTS
SET-COR-APM  8, 1853 % LPACF (B)   DATA,  6*26 POINTS
SET-COR-APM  7, 2009 % LPACF (B+C) DATA,  1*26 POINTS

SET-COR-APM  6, 1181 % FPP DATA, 80(S+B) + 40(B) + 6(B+C) = 126 POINTS

SET-COR-APM  5,  160 % A/C VARIANCE PROFILE DATA, 16-1+61=76 POINTS
SET-COR-APM  4, 2035 % SCANCOUNT ADDRESS

% APM1-APM3 ARE WORKING REGISTERS

SET-COR-APM  0,1     %APMINCR


SET-COR-DATAIO 2036

%  **FOR THE DETAILED STRUCTURE OF THE DATA DUMP, SEE THE CP-1-J:DESC**

HOLD 10

SET-ADC-INT 1,10             %FPP1
SET-ADC-INT 2,10             %FPP2
SET-ADC-INT 3,10             %LP1
SET-ADC-INT 4,10             %LP2
SET-ADC-INT 5,21             %E1 PP/ALTCODE
SET-ADC-INT 6,21             %E2 PP/ALTCODE
SET-ADC-INT 7,21             %E3 PP/ALTCODE
SET-ADC-INT 8,21             %E4 PP/ALTCODE


SET-BUF-MEM 1,1128,3     % (S+B) FPP DATA,                         =240 S.
SET-BUF-MEM 2,1368,3     % (B) + (B+C) FPP DATA, 120 + 18          =138 S.
SET-BUF-MEM 3,   0,3     % (S+B) LPACF DATA, 2*25 + 21*15 SAMPLES  =365 S.
SET-BUF-MEM 4, 365,3     % (B) + (B+C) LPACF DATA, 6*(2*25 + 15)+15=405 S.
SET-BUF-MEM 5, 860,3     % (S+B) PP1 + A/C ACF DATA                =166 S.
SET-BUF-MEM 8,1026,3     % (B) + (B+C) PP, A/C DATA                =102 S.
SET-BUF-MEM 7, 770,3     % (S+B) PP2 DATA                          = 90 S.
SET-BUF-MEM 6,4000,3     % IDLE

SET-BUF-MEM 1,1368,2
SET-BUF-MEM 2,1128,2
SET-BUF-MEM 3, 365,2
SET-BUF-MEM 4,   0,2
SET-BUF-MEM 7,1026,2
SET-BUF-MEM 6, 860,2
SET-BUF-MEM 5,4000,2
SET-BUF-MEM 8, 770,2

SET-BUF-MEM 1,1128,1
SET-BUF-MEM 2,1368,1
SET-BUF-MEM 3,   0,1
SET-BUF-MEM 4, 365,1
SET-BUF-MEM 5, 770,1
SET-BUF-MEM 8,4000,1
SET-BUF-MEM 7, 860,1
SET-BUF-MEM 6,1026,1

SET-BUF-MEM 1,1368,0
SET-BUF-MEM 2,1128,0
SET-BUF-MEM 3, 365,0
SET-BUF-MEM 4,   0,0
SET-BUF-MEM 7,4000,0
SET-BUF-MEM 6, 770,0
SET-BUF-MEM 5,1026,0
SET-BUF-MEM 8, 860,0
%                     %THE LAST USED ADDRESS IS  1505

HOLD 10               % GIVE TIME TO LOAD HARDWARE UP TO HERE

START-CORR

SYNC 10

START-RA-CON 10,1,5
SYNC 10

ENABLE-DMA U,U
?
?   ------------------------------------
?   SWITCH TX RF ON, CHECK DATA QUALITY.
?     RECORDING STARTS IN 5 MINUTES.
?
?   FOR DOPPLER CALIBR GIVE 'GOTO 2' COMMAND
?   ----------------------------------------
?
SYNC 280

BLOCK 1
DOPPLER-CALIBRATION OFF
START-PROGRAM INTCP,,,    % START INTEGRATION PROGRAM
START-PROGRAM VELOCTY,Y,,,    % START REAL TIME VECTOR-VELOCITY PROGRAM

DO -1
  NEW-FILE
?
? ***************************************
? * TO STOP THE EXPERIMENT USE 'LIMIT,,'
?      NOT 'STOP-EXP,,' (DOPPLER) *
? ***************************************
?
  DO 6
    SET-PARAMETER I20,1   %TO FORCE 5-MIN INTEGRATIONS IN VELOCTY
    SYNC 150
    SET-PARAMETER I20,5
    SYNC 150
  ENDDO
ENDDO

BLOCK 2

STOP-RECORDING Y

?
? >>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<
? THE DOPPLER-CALIBRATION WILL START IN 1 MIN,
?             PLEASE MONITOR
?
? >>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<
?
SYNC 15
STOP-PROGRAM INTCP

WRITE-FILE (CP-EI)CP-DOPPLER:DESC
SET-SIGNAL-ATTENUATOR  X,3
SET-FREQ  9,D                   % THIS SETS CH'S 1-8 TO 151.6 MHZ
DOPPLER-CALIBRATION ON
SYNC 45
START-RECORDING
DO 2
?  >>> DOPPLER CALIBRATION ON <<<
?  >> ACTIVATE WINDOW 13 ON RTGRAPH TO SEE SPECTRA <<
? TO STOP THE PROGRAM USE "STOP-EXPERIMENT"
?        COMMAND OR WAIT 10 MINUTES
? TO CONTINUE WITH CP-1 USE "GOTO-BLOCK 3"
?
  SYNC 300
ENDDO
DOPPLER-CALIBRATION OFF
STOP-EXPERIMENT,,,,

% STANDARD STOP OF EXPERIMENT AFTER DOPPLER-CALIBRATION

BLOCK 3
STOP-RECORDING  Y
DUMP-LOGG-FILE
DOPPLER-CALIBRATION OFF

SET-FREQ 1, F4
SET-FREQ 2, F5
SET-FREQ 3, F0
SET-FREQ 4, F1
SET-FREQ 5, F6
SET-FREQ 6, F7
SET-FREQ 7, F8
SET-FREQ 8, F9

SET-SIG-ATT X,3

?  >>> DOPPLER CALIBRATION OFF, PREPARING FOR CP-1 SETTINGS <<
?  TO START CP-1 RECORDING USE "GOTO-BLOCK 1 <HR>,<MIN>,<SEC>"
?
% WAIT HERE FOR EITHER STOP-EXPERIMENT OR GOTO-BLOCK 2 (OR 1)

DO -1
?  >>> YOU ARE IN BLOCK 3  <<<
?  >>  GOTO-BLOCK 1  OR STOP-EXPERIMENT  <<<
SYNC 600
ENDDO
%
%
STOP-EXPERIMENT,,
E-O-E
