function dum=t2ps(site,rc,p)
if nargin<2, rc=0; end
if nargin<3, p=0; end
if rc==0
 apustr='';
else
 apustr=['_' int2str(rc)];
end
t2psfile=['t_to_ps.txt' apustr '.' lower(site)];
t_to_ps=load(t2psfile,'-ascii');
if site=='r'
  d=find(rem(t_to_ps(:,4),1)); t_to_ps(d,:)=[];
end
p_offsetppd=0;
td_t1=t_to_ps(:,1)';
td_t2=t_to_ps(:,2)';
td_am=t_to_ps(:,3)';
td_ch=t_to_ps(:,4)';
if p
 ch_adcint=[.4];
 ch_filter={'b800d6.fir'};
 ch_f=[12];
 site='P';
 p_rep=357120;
 apustr='_2';
elseif site=='l'
 ch_adcint=[25 25];
 ch_filter={'b14d375.fir' 'b14d375.fir'};
 ch_f=[500.3 500.5];
 p_rep=400000;
elseif site=='t'
 ch_adcint=[2 2];
 ch_filter={'b250d30.fir' 'b250d30.fir'};
 ch_f=[13 14];
 p_rep=2299904;
elseif site=='r'
 ch_adcint=[10];
 ch_filter={'b35d150.fir'};
 ch_f=[12];
 p_rep=357120;
 if rc==2
  td_ch=t_to_ps(:,4)';
  d=find(td_ch==4); td_ch(d)=12;
  ch_adcint=[20];
  ch_filter={'b25d300.fir'};
 end
elseif site=='v'
 ch_adcint=[3 3];
 ch_filter={'b170d45.fir' 'b170d45.fir'};
 ch_f=[4 5];
 p_rep=3145728;
else
 error('giveup')
end
for f=1:length(ch_f)
 d=find(td_ch==ch_f(f));
 td_ch(d)=f;
end
name_expr='lace';
name_site=upper(site);
save_PS
