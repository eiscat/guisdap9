% GUISDAP v1.50   94-03-10 Copyright Asko Huuskonen, Markku Lehtinen 
% 
% CP3Fvcinit.m
T=p_rep/2;
vc_ch=[1 2  3  4   1   2   3   4   5   6   7   8];
vc_t1=[0 0  0  0   T   T   T   T   0   0   0   0];
vc_t2=[T T  T  T 2*T 2*T 2*T 2*T 2*T 2*T 2*T 2*T];
