%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   GUP ver. 1.1       Sodankyla  17 Aug 1990                       %
%   copyright Markku Lehtinen, Asko Huuskonen, Matti Vallinkoski    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cp1vcinit.m
vc_ch=[1 2  3  4   5   6   7   8   5   6   7   8];
vc_t1=[0 0  1  1 2.5 2.5 2.5 2.5   0   0   0   0]*1000;
vc_t2=[6 6 12 12  13  13  13  13 2.5 2.5 2.5 2.5]*1000;
