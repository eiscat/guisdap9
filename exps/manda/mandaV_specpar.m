% The scale parameters
p_T0=100;
p_N0=1e10;
p_om=sinh(-4:0.004:4.001)';
p_R0=500;
