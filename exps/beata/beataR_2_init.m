% cp1lR_init.m: Initialization for cp1lr
% GUISDAP v1.65
% 
% Radar frequency and basic time unit
% Klystron installed 2000: 926-930.5 MHz
                                     
ch_fradar=224e6;
ch_gain=10^3.54;
