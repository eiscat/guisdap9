% guispert.m: special experiment specific hacks
% GUISDAP v8.5   06-05-30 Copyright EISCAT
%
% See also: GUISPERT GUIZARD
%
if name_site=='L'
 if length(d_data)>11370
  calTemp=[163*ones(1,17) 228*ones(1,17)];
  if all(ch_gain==ch_gain(1))
    if isempty(a_code)
      a_satch.clutter=repmat(a_satch.clutter,1,2);
      a_satch.repair=repmat(a_satch.repair,1,2);
    end
    if d_date>datenum(2010,4,11)
      ch_gain(3:4)=ch_gain(3:4)*1.1;
    elseif d_date>datenum(2010,4,1)
      ch_gain=ch_gain.*[.5 .5 0.88 0.88], % Tx on 32m
    elseif d_date>datenum(2010,2,23)
      ch_gain=ch_gain.*[.3 .3 .7 .7];
    else
      ch_gain(3:4)=ch_gain(3:4)*0.8;
    end
  end
 end
 %glp=1182;
 %grps=[1 1 lpg_h(1);2 1180 lpg_h(1)+lpg_w(1)/2
 %     1181 1182 lpg_h(1182)];
 %gaincorrect(glp,grps)
end
if a_pponly
 d_time=timeconv(timeconv(d_time,'utc2tai')+[min(a_code)-3;max(a_code)-max(lpg_code(lpg_bcs=='s'))]*max(vc_penvo)*p_dtau/1e6,'tai2utc');
end
